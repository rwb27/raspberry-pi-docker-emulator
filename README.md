# Raspberry Pi docker test container

This will eventually be a script that creates a docker image on which we can run Python code intended for a Raspberry Pi. The idea is that this lets us test the OpenFlexure software on something that closely approximates the target system, so we don't get snarled up in multi-platform dependency management (i.e. we can be more relaxed about locking deps separately for Pi and other systems).

